/************************************************************************
 * a3 - an assembler for the sOSage operating system                     *
 * Copyright (C) 2022-2023 Mark Williams                                 *
 *                                                                       *
 * This file is part of a3.                                              *
 *                                                                       *
 * a3 is free software: you can redistribute it and/or modify            *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * a3 is distributed in the hope that it will be useful,                 *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with a3.  If not, see <https://www.gnu.org/licenses/>.          *
 *************************************************************************/

#include "a3.h"
#include "hash.h"

void assemble(assembly_instr *ass) {

	// Get opcode
	ass->code[0] = ass->instr->opcode; 
	
	// Default copy_bytes to nbytes
	ass->copy_bytes = ass->instr->nbytes;
	
	// Adjust opcode according to specified rule
	ass->instr->apply_rule(ass);

}

short get_label_offset(assembly_instr *curr, char *label) {
	while(curr != NULL) {
		if(!curr->is_instr && strcmp(curr->text, label)==0) {
			return curr->offset;
		}
		curr = curr->next;
	}
}

void second_pass(assembly_instr *curr) {
	char label[40];
	short label_offset;
	assembly_instr *head = curr;
	while(curr != NULL) {
		if(curr->is_instr && curr->dest->type=='l') {
			strcpy(label,curr->dest->text);
			strcat(label,":");
			label_offset = get_label_offset(head, label);
			label_offset -= curr->next->offset;
			curr->code[curr->copy_bytes-1] = label_offset;
			printf("%10s | %d\n", &label, label_offset);
		}
		curr = curr->next;
	}
}

void use_opcode(assembly_instr *ass) {
	; // do nothing - no opcode adjustments
}

void jcc(assembly_instr *ass) {
	ass->code[1] = 0; // ass->dest->value.ch[0];
}

void imm2ax(assembly_instr *ass) {
	// redirect to imm2reg if src is 1 byte and dest is 2
	unsigned short l;
	char mnemonic[10];
	if(ass->dest->width==1 && ass->src->value.sn > -129 && ass->src->value.sn < 128) {
		strcpy(mnemonic,ass->instr->mnemonic);
		l = strlen(mnemonic);
		mnemonic[l-2]='r';
		printf("Re-loading: %s\n",mnemonic);
		ass->instr = search_hash(mnemonic);
		assemble(ass);
		return;
	}
	// w
	ass->code[0] |= ass->dest->width;
	// data
	ass->code[1] = ass->src->value.ch[0];
	if(ass->dest->width) {
		ass->code[2] = ass->src->value.ch[1];
	}
	ass->copy_bytes = ass->instr->nbytes + 1 + (short) ass->dest->width;
}

void plus_reg(assembly_instr *ass) {
	// add the value of the dest reg
	ass->code[0] |= ass->dest->value.ch[0];
}

void load_fake_instr(assembly_instr *ass) {
	
	unsigned short l;
	char mnemonic[10];

	strcpy(mnemonic,ass->instr->mnemonic);
	l = strlen(mnemonic);
	mnemonic[l++] = '.';
	mnemonic[l++] = ass->dest->type;
	mnemonic[l++] = ass->src->type;
	mnemonic[l] = '\0';
	printf("Loading: %s\n",mnemonic);
	ass->instr = search_hash(mnemonic);
	assemble(ass);
}

void imm2reg(assembly_instr *ass) {
	// w
	ass->code[0] |= ass->dest->width;
	// 2nd byte
	ass->code[1] = ass->instr->opcode2;
	ass->code[1] <<=3;
	ass->code[1] |= 0b11000000; // mod
	// dest reg
	ass->code[1] |= ass->dest->value.ch[0];
	// data
	ass->code[2] = ass->src->value.ch[0];
	if(ass->dest->width==0) {
		ass->copy_bytes = ass->instr->nbytes + 1;
	} else if(ass->src->value.sn > -129 && ass->src->value.sn < 128) {
		ass->code[0] |= 2;
		ass->copy_bytes = ass->instr->nbytes + 1;
	} 	else {
		ass->code[3] = ass->src->value.ch[1];
		ass->copy_bytes = ass->instr->nbytes + 2;
	}
}

void mov_imm2reg(assembly_instr *ass) {
	// w
	ass->code[1] = ass->dest->width;
	ass->code[1] <<=3;
	ass->code[0] |= ass->code[1];
	// dest reg
	ass->code[0] |= ass->dest->value.ch[0];
	// data
	ass->code[1] = ass->src->value.ch[0];
	if(ass->dest->width)
		ass->code[2] = ass->src->value.ch[1];
	ass->copy_bytes = ass->instr->nbytes + 1 + (short) ass->dest->width;
}

void imm2mem(assembly_instr *ass) { 
	// w
	ass->code[0] |= ass->dest->width;
	// 2nd byte
	ass->code[1] = ass->instr->opcode2;
	ass->code[1] <<=3;
	ass->code[1] |= 0b00000110; // mod
	// dest reg
	ass->code[1] |= ass->dest->value.ch[0];
	// data
	ass->code[2] = ass->dest->value.ch[0];
	ass->code[3] = ass->dest->value.ch[1];
	ass->code[4] = ass->src->value.ch[0];
	if(ass->dest->width==0) {
		ass->copy_bytes = ass->instr->nbytes + 3;
	} else if(ass->src->value.sn > -129 && ass->src->value.sn < 128) {
		ass->code[0] |= 2;
		ass->copy_bytes = ass->instr->nbytes + 3;
	} 	else {
		ass->code[5] = ass->src->value.ch[1];
		ass->copy_bytes = ass->instr->nbytes + 4;
	}
}

void mov_imm2mem(assembly_instr *ass) { 
	// w
	ass->code[0] |= ass->dest->width;
	// 2nd byte
	ass->code[1] = 0b00000110;	
	// data
	ass->code[2] = ass->dest->value.ch[0];
	ass->code[3] = ass->dest->value.ch[1];
	ass->code[4] = ass->src->value.ch[0];
	if(ass->dest->width) {
		ass->code[5] = ass->src->value.ch[1];
	}
	ass->copy_bytes = ass->instr->nbytes + 3 + (short) ass->dest->width;
}

void reg2reg(assembly_instr *ass) { 
	// w
	ass->code[0] |= ass->dest->width;
	// src reg
	ass->code[1] = ass->src->value.ch[0];
	ass->code[1] <<= 3;
	// dest reg
	ass->code[1] |= ass->dest->value.ch[0];
	// mode
	ass->code[1] |= 0b11000000;
}

void reg2mem(assembly_instr *ass) { 
	// w
	ass->code[0] |= ass->src->width;
	// dest reg
	ass->code[1] = ass->src->value.ch[0];
	ass->code[1] <<= 3;
	// mode
	ass->code[1] |= 0b00000110;
	// data
	ass->code[2] = ass->dest->value.ch[0];
	ass->code[3] = ass->dest->value.ch[1];
	ass->copy_bytes = ass->instr->nbytes + 2;
}

void mem2reg(assembly_instr *ass) { 
	// w
	ass->code[0] |= ass->dest->width;
	// dest reg
	ass->code[1] = ass->dest->value.ch[0];
	ass->code[1] <<= 3;
	// mode
	ass->code[1] |= 0b00000110;
	// data
	ass->code[2] = ass->src->value.ch[0];
	ass->code[3] = ass->src->value.ch[1];
	ass->copy_bytes = ass->instr->nbytes + 2;
}

void ax2mem(assembly_instr *ass) { 
	// w
	ass->code[0] |= ass->src->width;
	// data
	ass->code[1] = ass->dest->value.ch[0];
	ass->code[2] = ass->dest->value.ch[1];
	ass->copy_bytes = ass->instr->nbytes + 2;
}

void mem2ax(assembly_instr *ass) { 
	// w
	ass->code[0] |= ass->dest->width;
	// data
	ass->code[1] = ass->src->value.ch[0];
	ass->code[2] = ass->src->value.ch[1];
	ass->copy_bytes = ass->instr->nbytes + 2;
}

void reg2seg(assembly_instr *ass) { 
	// src reg
	ass->code[1] = ass->dest->value.ch[0];
	ass->code[1] <<= 3;
	// dest reg
	ass->code[1] |= ass->src->value.ch[0];
	// mode
	ass->code[1] |= 0b11000000;
}

void seg2reg(assembly_instr *ass) { 
	// src reg
	ass->code[1] = ass->src->value.ch[0];
	ass->code[1] <<= 3;
	// dest reg
	ass->code[1] |= ass->dest->value.ch[0];
	// mode
	ass->code[1] |= 0b11000000;
}

