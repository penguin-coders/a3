## v0.2.1 - 2023-05-02

Implemented remaining conditional jump (jcc) instructions.

**Added:**
- jo
- jno
- jb   
- jnb  
- jnz
- jbe
- ja
- js
- jns
- jp
- jnp
- jl
- jnl
- jle
- jnle

## v0.2.1 - 2023-05-01

Implemented two-pass assembly and the jz instruction.
**Added:**
- get\_label\_offset and second\_pass functions for two-pass assmebly
- Implentation of jz

**Changed:**
- assembly\_instr now has offset and is\_instr fields

## v0.2.0 - 2022-10-16

Refactored code in preparation for two pass assembly. Also added functionality to print the source code and object code.

**Added:**
- Command line option to print source code adjacent to object code

**Changed:**
- Updates to README file
- Refactored code to store code in a linked list

## v0.1.0 - 2022-10-15

Implemented and, or, xor

**Added:**
- Implementation for or
- Implementation for and
- Implementation for xor

**Changed:**
- Increased instruction array size

## v0.0.11 - 2022-10-13

Implemented sbb and adc

**Added:**
- Implemented sbb
- Implemented adc

## v0.0.10 - 2022-10-12

Implemented cmp

**Added:**
- Implementation for cmp

**Changed:**
- Increased instruction array size

## v0.0.9 - 2022-10-11

Made changes necessary for implementing additional arithmetic functions and implemented sum.

**Added:**
- Implementation for sum

**Changed:**
- Added second opcode field to instruction struct

**Bug fixes:**
- Fixed imm2mem function for sub and add

## v0.0.8 - 2022-10-10

Initial implementation of push and pop

**Added:**
- Implementation of push for main registers
- Implementation of pop for main registers

## v0.0.7 - 2022-10-10

Implemented inc and dec

**Added:**
- Rules for adding the value of a register to an opcode (plus\_reg)
- Implementation for inc
- Implementation for dec

## v0.0.6 - 2022-10-10

Added rules to implement add mnemonic in full.

**Added:**
- Rules for add instruction

**Changed:**
- Tweaked Makefile to hide output when computing diffs.

**Bug fixes:**
- is\_num now handles negative numbers properly
- Fixed a bug in try\_mem function


## v0.0.5 - 2022-05-01

Added test target in Makefile which assembles test code using both nasm and a3 and compares the resulting binaries by hexdumping them and piping into diff. diff will output a patch for each comparison in testcode/diffs.

**Added:**
- Test target in Makefile

## v0.0.4 - 2022-04-24

Completed mov instruction, amended hash to deal with collissions, re-factored code

**Added:**
- Remaining rules for mov instruction
- External chaining in hash table (to deal with collissions)

**Changed:**
- fake instructions now contain a '.'
- operand struct now has width member
- changed assembly rules to use function pointers instead of switch

**Bug fixes:**
- fixed bug in imm->reg rule
- a register type was just ax, now also al

## v0.0.3 - 2022-04-16

Added nop instruction and hash table to store instruction hash

**Added:**
- Hash table to store instructions
- nop instruction

**Changed:**
- Removed .pattern from assembly_instr and replace with .rule in instruction struct

## v0.0.2 - 2022-04-15

Code amended to write assembled code out to binary file and added command line options

**Added:**
- Code to output assembled code
- command line options for verbose mode, filename and output filename

**Changed:**
- Conditionalised output with verbose (-v) option
## v0.0.1 - 2022-04-14

First version - compiles register-register 'mov' commands.

**Added:**
- Code to compile mov instructions
- README, CHANGELOG, CONTRIBUTING
- A Makefile for assembling with `make`
