/************************************************************************
 * a3 - an assembler for the sOSage operating system                     *
 * Copyright (C) 2022-2023 Mark Williams                                 *
 *                                                                       *
 * This file is part of a3.                                              *
 *                                                                       *
 * a3 is free software: you can redistribute it and/or modify            *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * a3 is distributed in the hope that it will be useful,                 *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with a3.  If not, see <https://www.gnu.org/licenses/>.          *
 *************************************************************************/

#include "hash.h"

void build_hash(instruction *ip) {
	
	init_hash();

	mk_instruction(ip, "nop", 0x90, 0, use_opcode, 1);	insert_hash(ip++);
	mk_instruction(ip, "inc", 0x40, 0, plus_reg, 1);	insert_hash(ip++);
	mk_instruction(ip, "dec", 0x48, 0, plus_reg, 1);	insert_hash(ip++);
	mk_instruction(ip, "push", 0x50, 0, plus_reg, 1);	insert_hash(ip++);
	mk_instruction(ip, "pop", 0x58, 0, plus_reg, 1);	insert_hash(ip++);
	
	mk_instruction(ip, "jo", 0x70, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jno", 0x71, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jb", 0x72, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jnb", 0x73, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jz", 0x74, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jnz", 0x75, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jbe", 0x76, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "ja", 0x77, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "js", 0x78, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jns", 0x79, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jp", 0x7a, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jnp", 0x7b, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jl", 0x7c, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jnl", 0x7d, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jle", 0x7e, 0, jcc, 2); insert_hash(ip++);
	mk_instruction(ip, "jnle", 0x7f, 0, jcc, 2); insert_hash(ip++);

	mk_instruction(ip, "mov", 0x00, 0, load_fake_instr, 0);	insert_hash(ip++);
	mk_instruction(ip, "mov.ri", 0xb0, 0, mov_imm2reg, 1); insert_hash(ip++);
	mk_instruction(ip, "mov.ai", 0xb0, 0, mov_imm2reg, 1); insert_hash(ip++); // = mov.ri
	mk_instruction(ip, "mov.mi", 0xc6, 0, mov_imm2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "mov.rr", 0x88, 0, reg2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "mov.ar", 0x88, 0, reg2reg, 2); insert_hash(ip++); // = mov.rr
	mk_instruction(ip, "mov.ra", 0x88, 0, reg2reg, 2); insert_hash(ip++); // = mov.rr
	mk_instruction(ip, "mov.mr", 0x88, 0, reg2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "mov.rm", 0x8a, 0, mem2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "mov.ma", 0xa2, 0, ax2mem, 1); insert_hash(ip++);
	mk_instruction(ip, "mov.am", 0xa0, 0, mem2ax, 1); insert_hash(ip++);
	mk_instruction(ip, "mov.sr", 0x8e, 0, reg2seg, 2); insert_hash(ip++);
	mk_instruction(ip, "mov.rs", 0x8c, 0, seg2reg, 2); insert_hash(ip++);

	mk_instruction(ip, "add", 0x00, 0, load_fake_instr, 0);	insert_hash(ip++);
	mk_instruction(ip, "add.ri", 0x80, 0, imm2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "add.ai", 0x04, 0, imm2ax, 1); insert_hash(ip++); 
	mk_instruction(ip, "add.mi", 0x80, 0, imm2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "add.rr", 0x00, 0, reg2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "add.ar", 0x00, 0, reg2reg, 2); insert_hash(ip++); // = add.rr
	mk_instruction(ip, "add.ra", 0x00, 0, reg2reg, 2); insert_hash(ip++); // = add.rr
	mk_instruction(ip, "add.mr", 0x00, 0, reg2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "add.ma", 0x00, 0, reg2mem, 2); insert_hash(ip++); // = add.mr
	mk_instruction(ip, "add.rm", 0x02, 0, mem2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "add.am", 0x02, 0, mem2reg, 2); insert_hash(ip++); // = add.rm
	
	mk_instruction(ip, "or", 0x00, 1, load_fake_instr, 0);	insert_hash(ip++);
	mk_instruction(ip, "or.ri", 0x80, 1, imm2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "or.ai", 0x0c, 1, imm2ax, 1); insert_hash(ip++); 
	mk_instruction(ip, "or.mi", 0x80, 1, imm2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "or.rr", 0x08, 1, reg2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "or.ar", 0x08, 1, reg2reg, 2); insert_hash(ip++); // = or.rr
	mk_instruction(ip, "or.ra", 0x08, 1, reg2reg, 2); insert_hash(ip++); // = or.rr
	mk_instruction(ip, "or.mr", 0x08, 1, reg2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "or.ma", 0x08, 1, reg2mem, 2); insert_hash(ip++); // = or.mr
	mk_instruction(ip, "or.rm", 0x0a, 1, mem2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "or.am", 0x0a, 1, mem2reg, 2); insert_hash(ip++); // = or.rm

	mk_instruction(ip, "adc", 0x00, 2, load_fake_instr, 0);	insert_hash(ip++);
	mk_instruction(ip, "adc.ri", 0x80, 2, imm2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "adc.ai", 0x14, 2, imm2ax, 1); insert_hash(ip++); 
	mk_instruction(ip, "adc.mi", 0x80, 2, imm2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "adc.rr", 0x10, 2, reg2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "adc.ar", 0x10, 2, reg2reg, 2); insert_hash(ip++); // = adc.rr
	mk_instruction(ip, "adc.ra", 0x10, 2, reg2reg, 2); insert_hash(ip++); // = adc.rr
	mk_instruction(ip, "adc.mr", 0x10, 2, reg2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "adc.ma", 0x10, 2, reg2mem, 2); insert_hash(ip++); // = adc.mr
	mk_instruction(ip, "adc.rm", 0x12, 2, mem2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "adc.am", 0x12, 2, mem2reg, 2); insert_hash(ip++); // = adc.rm
	
	mk_instruction(ip, "sbb", 0x00, 3, load_fake_instr, 0);	insert_hash(ip++);
	mk_instruction(ip, "sbb.ri", 0x80, 3, imm2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "sbb.ai", 0x1c, 3, imm2ax, 1); insert_hash(ip++); 
	mk_instruction(ip, "sbb.mi", 0x80, 3, imm2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "sbb.rr", 0x18, 3, reg2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "sbb.ar", 0x18, 3, reg2reg, 2); insert_hash(ip++); // = sbb.rr
	mk_instruction(ip, "sbb.ra", 0x18, 3, reg2reg, 2); insert_hash(ip++); // = sbb.rr
	mk_instruction(ip, "sbb.mr", 0x18, 3, reg2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "sbb.ma", 0x18, 3, reg2mem, 2); insert_hash(ip++); // = sbb.mr
	mk_instruction(ip, "sbb.rm", 0x1a, 3, mem2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "sbb.am", 0x1a, 3, mem2reg, 2); insert_hash(ip++); // = sbb.rm
	
	mk_instruction(ip, "and", 0x00, 4, load_fake_instr, 0);	insert_hash(ip++);
	mk_instruction(ip, "and.ri", 0x80, 4, imm2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "and.ai", 0x24, 4, imm2ax, 1); insert_hash(ip++); 
	mk_instruction(ip, "and.mi", 0x80, 4, imm2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "and.rr", 0x20, 4, reg2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "and.ar", 0x20, 4, reg2reg, 2); insert_hash(ip++); // = and.rr
	mk_instruction(ip, "and.ra", 0x20, 4, reg2reg, 2); insert_hash(ip++); // = and.rr
	mk_instruction(ip, "and.mr", 0x20, 4, reg2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "and.ma", 0x20, 4, reg2mem, 2); insert_hash(ip++); // = and.mr
	mk_instruction(ip, "and.rm", 0x22, 4, mem2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "and.am", 0x22, 4, mem2reg, 2); insert_hash(ip++); // = and.rm

	mk_instruction(ip, "sub", 0x00, 5, load_fake_instr, 0);	insert_hash(ip++);
	mk_instruction(ip, "sub.ri", 0x80, 5, imm2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "sub.ai", 0x2c, 5, imm2ax, 1); insert_hash(ip++); 
	mk_instruction(ip, "sub.mi", 0x80, 5, imm2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "sub.rr", 0x28, 5, reg2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "sub.ar", 0x28, 5, reg2reg, 2); insert_hash(ip++); // = sub.rr
	mk_instruction(ip, "sub.ra", 0x28, 5, reg2reg, 2); insert_hash(ip++); // = sub.rr
	mk_instruction(ip, "sub.mr", 0x28, 5, reg2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "sub.ma", 0x28, 5, reg2mem, 2); insert_hash(ip++); // = sub.mr
	mk_instruction(ip, "sub.rm", 0x2a, 5, mem2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "sub.am", 0x2a, 5, mem2reg, 2); insert_hash(ip++); // = sub.rm
	
	mk_instruction(ip, "xor", 0x00, 6, load_fake_instr, 0);	insert_hash(ip++);
	mk_instruction(ip, "xor.ri", 0x80, 6, imm2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "xor.ai", 0x34, 6, imm2ax, 1); insert_hash(ip++); 
	mk_instruction(ip, "xor.mi", 0x80, 6, imm2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "xor.rr", 0x30, 6, reg2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "xor.ar", 0x30, 6, reg2reg, 2); insert_hash(ip++); // = xor.rr
	mk_instruction(ip, "xor.ra", 0x30, 6, reg2reg, 2); insert_hash(ip++); // = xor.rr
	mk_instruction(ip, "xor.mr", 0x30, 6, reg2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "xor.ma", 0x30, 6, reg2mem, 2); insert_hash(ip++); // = xor.mr
	mk_instruction(ip, "xor.rm", 0x32, 6, mem2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "xor.am", 0x32, 6, mem2reg, 2); insert_hash(ip++); // = xor.rm

	mk_instruction(ip, "cmp", 0x00, 7, load_fake_instr, 0);	insert_hash(ip++);
	mk_instruction(ip, "cmp.ri", 0x80, 7, imm2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "cmp.ai", 0x3c, 7, imm2ax, 1); insert_hash(ip++); 
	mk_instruction(ip, "cmp.mi", 0x80, 7, imm2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "cmp.rr", 0x38, 7, reg2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "cmp.ar", 0x38, 7, reg2reg, 2); insert_hash(ip++); // = cmp.rr
	mk_instruction(ip, "cmp.ra", 0x38, 7, reg2reg, 2); insert_hash(ip++); // = cmp.rr
	mk_instruction(ip, "cmp.mr", 0x38, 7, reg2mem, 2); insert_hash(ip++);
	mk_instruction(ip, "cmp.ma", 0x38, 7, reg2mem, 2); insert_hash(ip++); // = cmp.mr
	mk_instruction(ip, "cmp.rm", 0x3a, 7, mem2reg, 2); insert_hash(ip++);
	mk_instruction(ip, "cmp.am", 0x3a, 7, mem2reg, 2); insert_hash(ip++); // = cmp.rm
}
