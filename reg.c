/*************************************************************************
 * a3 - an assembler for the sOSage operating system                     *
 * Copyright (C) 2022-2023 Mark Williams                                 *
 *                                                                       *
 * This file is part of a3.                                              *
 *                                                                       *
 * a3 is free software: you can redistribute it and/or modify            *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * a3 is distributed in the hope that it will be useful,                 *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with a3.  If not, see <https://www.gnu.org/licenses/>.          *
 *************************************************************************/

#include "a3.h"

int try_reg(operand *oper) {

	word reg;
	short num, w;

	if(oper->text[2]!='\0') 
		return 0; // not a register
	
	reg.ch[0] = oper->text[0];
	reg.ch[1] = oper->text[1];

	switch(reg.n) {
		case 0x6c61: // al
		case 0x7861: // ax
		case 0x7365: // es
			num = 0; break;
		case 0x6c63: // cl
		case 0x7863: // cx
		case 0x7363: // cs
			num = 1; break;
		case 0x6c64: // dl
		case 0x7864: // dx
		case 0x7373: // ss
			num = 2; break;
		case 0x6c62: // bl
		case 0x7862: // bx
		case 0x7364: // ds
			num = 3; break;
		case 0x6861: // ah
		case 0x7073: // sp
		case 0x7366: // fs
			num = 4; break;
		case 0x6863: // ch
		case 0x7062: // bp
		case 0x7367: // gs
			num = 5; break;
		case 0x6864: // dh
		case 0x6973: // si
			num = 6; break;
		case 0x6862: // bh
		case 0x6964: // di
			num = 7; break;
		default: return 0; // not a register
	}

	// Store reg num in low byte of value
	oper->value.n = num;

	// Set width
	if(reg.ch[1]=='h' || reg.ch[1]=='l') {
		oper->width = 0;
	} else {
		oper->width = 1;
	}
	
	// Set type
	if(reg.n==0x7861 || reg.n==0x6c61) {
		oper->type = 'a'; // ax, al
	} else if(reg.ch[1]=='s') {
		oper->type = 's'; // seg
	} else {
		oper->type = 'r'; // reg
	}

	return 1; // success
}

void check_reg(operand *oper, char *reg) {

	strcpy(oper->text,reg);
	
	if(try_reg(oper)) {
		printf("reg: %c%c | type: %c | w: %u | num: %u\n", oper->text[0], oper->text[1], oper->type, oper->value.ch[1], oper->value.ch[0]);
	}
}

