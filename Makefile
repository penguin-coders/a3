CFILES=$(wildcard *.c)
OBJECTS=$(patsubst %.c,%.o,$(CFILES))
BINS=a3

TESTDIR=testcode
TESTFILES=$(wildcard $(TESTDIR)/*.asm)
A3S=$(patsubst $(TESTDIR)/%.asm,$(TESTDIR)/a3/%,$(TESTFILES))
NASMS=$(patsubst $(TESTDIR)/%.asm,$(TESTDIR)/nasm/%,$(TESTFILES))
DIFFS=$(patsubst $(TESTDIR)/%.asm,$(TESTDIR)/diffs/%.patch,$(TESTFILES))

all: $(BINS) $(OBJECTS) 

$(BINS): $(OBJECTS)
	gcc -o $@ $^

%.o: %.c
	gcc -c $^

test: $(NASMS) $(A3S) $(DIFFS)
	
$(A3S): $(BINS)

$(TESTDIR)/nasm/%: $(TESTDIR)/%.asm
	nasm -f bin -o $@ $^

$(TESTDIR)/a3/%: $(TESTDIR)/%.asm
	./a3 -o $@ -f $^

$(TESTDIR)/diffs/%.patch: $(TESTDIR)/nasm/% $(TESTDIR)/a3/%
	@bash -c "diff -au <(hexdump -C $< ) <(hexdump -C $(word 2,$^) ) >$@; exit 0;"

clean:
	rm $(OBJECTS) $(BINS) $(A3S) $(NASMS) $(DIFFS)
