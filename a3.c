/*************************************************************************
 * a3 - an assembler for the sOSage operating system                     *
 * Copyright (C) 2022-2023 Mark Williams                                 *
 *                                                                       *
 * This file is part of a3.                                              *
 *                                                                       *
 * a3 is free software: you can redistribute it and/or modify            *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * a3 is distributed in the hope that it will be useful,                 *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with a3.  If not, see <https://www.gnu.org/licenses/>.          *
 *************************************************************************/

#include "a3.h"
#include "hash.h"

short verbose = 0;
short print = 0;

void new_ass(assembly_instr **curr) {
	assembly_instr *newass = malloc(sizeof(assembly_instr));
	newass->src = malloc(sizeof(instruction));
	newass->dest = malloc(sizeof(instruction));
	newass->next = NULL;
	newass->copy_bytes = 0;
	newass->is_instr = 1;
	if(*curr != NULL) {
		newass->offset = (*curr)->offset + (*curr)->copy_bytes;
		(*curr)->next = newass;
	} else {
	  newass->offset = 0;	
	}
	*curr = newass;
}

void print_list(assembly_instr *curr) {
	while(curr != NULL) {
		printf("%20s\t;", curr->text);
		for(int c=0; c < curr->copy_bytes; c++) {
			printf(" %02x",curr->code[c]);
		}
		printf("\t; offset:%d", curr->offset);
		if(verbose)
			printf("\t; is_instr:%d", curr->is_instr);
			printf("\t; instr:%u", curr->instr);
		printf("\n");
		curr = curr->next;
	}
}

int main(int argc, char *argv[]) {

   int opt;
	FILE *srcfile, *binfile;
	char *input_fname=NULL;
	char *output_fname=NULL;
	char *line_buf = NULL;
	size_t line_buf_size = 0;
	int line_size;
	int c;
	
	instruction instr[150];
	assembly_instr *ass = NULL;
	assembly_instr *head = NULL;

	// Read in command line options
	while((opt=getopt(argc,argv,"vpf:o:")) != -1) {
		switch(opt) {
         case 'v': verbose = 1; break;
			case 'p': print = 1; break;
         case 'f': input_fname = optarg; break;
			case 'o': output_fname = optarg; break;						 
      }
   }
   if(optind < argc && input_fname==NULL) 
      input_fname = argv[optind];
	
	// Open file
	if(input_fname==NULL || !(srcfile=fopen(input_fname,"r"))) {
		printf("File not found.\n");
		return 1;
	}

	// Setup instructions hash table
	build_hash(instr);
	
	// Loop through instructions in file
	while ((line_size=getline(&line_buf, &line_buf_size, srcfile)) >= 0) {

		// Create node in linked list for next instruction
		new_ass(&ass);
		if(head==NULL) head=ass;

		// Copy line from buffer to struct
		line_buf[line_size-1]='\0';
		strcpy(ass->text,line_buf);

		// Parse instruction
		if(verbose)
			printf("\nParsing '%s'...\n",ass->text);
		parse(ass);

		// Assemble
		if(ass->is_instr) {
			if(verbose)
				printf("Assembling...\n");
			assemble(ass);
		}

	}

	// Do second pass to fix jumps
	second_pass(head);

	if(print)
		print_list(head);

	if(verbose) {
		printf("Code assembled for all instructions:\n");
		ass = head;
		while(ass != NULL) {
			for(c=0; c<ass->copy_bytes;) 
				printf("%x ", ass->code[c++]);
			ass = ass->next;
		}
		printf("\n");
	}

	// Output code to file
	if(output_fname==NULL) {
		output_fname = (char *) malloc(6);
		strcpy(output_fname,"a.out");
	}
	
	binfile=fopen(output_fname,"wb");

	ass = head;
	while(ass != NULL) {
		fwrite(ass->code, ass->copy_bytes, 1, binfile);
		ass = ass->next;
	}

	return 0;

}
