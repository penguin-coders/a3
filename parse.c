/************************************************************************
 * a3 - an assembler for the sOSage operating system                     *
 * Copyright (C) 2022-2023 Mark Williams                                 *
 *                                                                       *
 * This file is part of a3.                                              *
 *                                                                       *
 * a3 is free software: you can redistribute it and/or modify            *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * a3 is distributed in the hope that it will be useful,                 *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with a3.  If not, see <https://www.gnu.org/licenses/>.          *
 *************************************************************************/

#include "a3.h"
#include "hash.h"

int is_hex(char *sn) {
	int len = strlen(sn);
	return (len>2 && sn[0]=='0' && sn[1]=='x'); 
}

int is_num(char *sn) {
	int i,len;
	len = strlen(sn);
	if(is_hex(sn)) {
		for(i=2; i<len; i++) {
			if(!((sn[i]>47 && sn[i]<58) || (sn[i]>64 && sn[i]<71) ||	(sn[i]>96 && sn[i]<103)))
				return 0;
		}
	} else {
		if((sn[0]<48 && sn[0]!=43 && sn[0]!=45) || sn[0]>57)
			return 0;
		for(i=1; i<len; i++) {
			if(sn[i]<48 || sn[i]>57) 
				return 0;
		}
	}
	return 1;
}

int get_val(char *str) {
	
	char *p;

	if(is_hex(str))
		return (int) strtol(str, &p, 16);

	return (int) strtol(str, &p, 10);
}

int try_mem(operand *oper) {
	char inner_text[10];
	int l;
	if(oper->text[0]=='[' && oper->text[strlen(oper->text)-1]==']') {
		l = strlen(oper->text)-2;
		strncpy(inner_text,oper->text+1,l);
		if(!is_num(inner_text))
				return 0;
		oper->value.n=get_val(inner_text);
		oper->type='m';
		return 1;
	} else {
		return 0;
	}
}

int try_imm(operand *oper) {
	if(!is_num(oper->text))
			return 0;
	oper->value.n=get_val(oper->text);
	oper->type='i';
	return 1;
}

void parse(assembly_instr *ass) {
		
   char *token;
	char text[40];

	if(verbose)
		printf("Text: %s | ", ass->text);

	strcpy(text, ass->text);
   
   token=strtok(text, " ,");
	if(token!=NULL) {
		if(token[strlen(token)-1]==':') {
			printf("LABEL! : %s\n", token);
			ass->is_instr = 0;
		} else {
			ass->instr = search_hash(token);
		}
	}
	
	token=strtok(NULL, " ,");
	if(token!=NULL) {
		if(strcmp(token,"byte")==0) {
			ass->dest->width=0;
			token=strtok(NULL, " ,");
		} else if(strcmp(token,"word")==0) {
			ass->dest->width=1;
			token=strtok(NULL, " ,");
		}
	   strcpy(ass->dest->text,token); 
	}

   token=strtok(NULL, " ,");
	if(token!=NULL)
	   strcpy(ass->src->text,token); 
	
	if(verbose)
		printf("mnemonic: %s | 1st operand: %s | 2nd operand: %s\n\n", ass->instr->mnemonic, ass->dest->text, ass->src->text);

	// Parse operands
	if(!try_reg(ass->dest))
	 	if(!try_imm(ass->dest))
			if(!try_mem(ass->dest)) {
				if(strlen(ass->dest->text)>0) {
					printf("LABEL (dest): %s\n",ass->dest->text);
					ass->dest->type='l';
				}
			}
	if(!try_reg(ass->src))
		if(!try_imm(ass->src))
			try_mem(ass->src);

}

