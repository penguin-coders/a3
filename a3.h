/************************************************************************
 * a3 - an assembler for the sOSage operating system                     *
 * Copyright (C) 2022-2023 Mark Williams                                 *
 *                                                                       *
 * This file is part of a3.                                              *
 *                                                                       *
 * a3 is free software: you can redistribute it and/or modify            *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * a3 is distributed in the hope that it will be useful,                 *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with a3.  If not, see <https://www.gnu.org/licenses/>.          *
 *************************************************************************/

#ifndef __A3_H
#define __A3_H

#define BUFFSIZE 100

/* includes */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

/* globals */
extern short verbose;
extern unsigned char *binbuff;
extern unsigned char *binptr;

/* typedefs */

typedef union {
	unsigned short n;
	short sn;
	unsigned char ch[2];
} word;

struct _ass_instr;

typedef struct instr {
	char mnemonic[7];
	unsigned char opcode;
	unsigned char opcode2;
	void (*apply_rule)(struct _ass_instr *);
	unsigned short nbytes;
	struct instr *next;
} instruction;

typedef struct {
	char text[10];
	char type;
	char width;
	word value;
} operand;

typedef struct _ass_instr {
	char text[40];
	unsigned char code[6];
	instruction	*instr;
	operand *src;
	operand *dest;
	unsigned short copy_bytes;
	unsigned int offset;
	unsigned short	is_instr;
	struct _ass_instr *next;
} assembly_instr;

/* functions */

int try_reg(operand *oper);
void check_reg(operand *oper, char *reg); 
void parse(assembly_instr *ass); 
void assemble(assembly_instr *ass); 
void second_pass(assembly_instr *curr);
short get_label_offset(assembly_instr *curr, char *label); 

// functions pointed to by apply_rule
void use_opcode(assembly_instr *ass); 
void jcc(assembly_instr *ass); 
void plus_reg(assembly_instr *ass);
void load_fake_instr(assembly_instr *ass); 
void imm2ax(assembly_instr *ass); 
void mov_imm2reg(assembly_instr *ass); 
void imm2reg(assembly_instr *ass); 
void mov_imm2mem(assembly_instr *ass); 
void imm2mem(assembly_instr *ass); 
void reg2reg(assembly_instr *ass); 
void reg2mem(assembly_instr *ass); 
void mem2reg(assembly_instr *ass); 
void ax2mem(assembly_instr *ass); 
void mem2ax(assembly_instr *ass); 
void reg2seg(assembly_instr *ass); 
void seg2reg(assembly_instr *ass); 

#endif
