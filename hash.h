/************************************************************************
 * a3 - an assembler for the sOSage operating system                     *
 * Copyright (C) 2022-2023 Mark Williams                                 *
 *                                                                       *
 * This file is part of a3.                                              *
 *                                                                       *
 * a3 is free software: you can redistribute it and/or modify            *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * a3 is distributed in the hope that it will be useful,                 *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with a3.  If not, see <https://www.gnu.org/licenses/>.          *
 *************************************************************************/

#ifndef __HASH_H
#define __HASH_H

#include "a3.h"

#define HASHTABLESIZE 127 
instruction *hash_table[HASHTABLESIZE];

/* Functions */
unsigned int hash(char *str);
void insert_hash(instruction *inst);
instruction *search_hash(char *mnemonic);
void init_hash(); 
void build_hash(instruction *ip);
void mk_instruction(instruction *ip, char *mnemonic, unsigned char opcode, unsigned char opcode2, void (*rule)(assembly_instr *), unsigned short nbytes); 

#endif
