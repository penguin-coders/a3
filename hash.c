/************************************************************************
 * a3 - an assembler for the sOSage operating system                     *
 * Copyright (C) 2022-2023 Mark Williams                                 *
 *                                                                       *
 * This file is part of a3.                                              *
 *                                                                       *
 * a3 is free software: you can redistribute it and/or modify            *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation, either version 3 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 * a3 is distributed in the hope that it will be useful,                 *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 * GNU General Public License for more details.                          *
 *                                                                       *
 * You should have received a copy of the GNU General Public licenses    *
 * along with a3.  If not, see <https://www.gnu.org/licenses/>.          *
 *************************************************************************/

#include "hash.h"

// N.B. build_hash is defined in build_hash.c

unsigned int hash(char *str) {
	
	unsigned int val=0;
	unsigned int i=0;

	while(str[i]!='\0') {
		val += str[i];
		val *= str[i];
		i++;	
	}
	
	return val % HASHTABLESIZE;
}

void insert_hash(instruction *inst) {
	unsigned int hsh = hash(inst->mnemonic);	
	printf("Instruction: %s, hash: %u\n", inst->mnemonic, hsh);
	if(hash_table[hsh]!=NULL) {
		printf("Hash collision\n");
		inst->next = hash_table[hsh];
	}
	hash_table[hsh] = inst;
}

instruction *search_hash(char *mnemonic) {
	instruction *ip = hash_table[hash(mnemonic)];
	while(strcmp(ip->mnemonic,mnemonic)!=0)
		ip = ip->next;
	return ip;
}

void mk_instruction(instruction *ip, char *mnemonic, unsigned char opcode, unsigned char opcode2, void (*rule)(assembly_instr *), unsigned short nbytes) {
	strcpy(ip->mnemonic,mnemonic);
	ip->opcode = opcode;
	ip->opcode2 = opcode2;
	ip->apply_rule = rule;
	ip->nbytes = nbytes;
}

void init_hash() {
	for(int i=0; i<HASHTABLESIZE; i++) {
		hash_table[i]=NULL;
	}
}


