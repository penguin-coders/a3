# a3

**Version 0.2.2** - [Change log](CHANGELOG.md)

a3 is an assembler, written in C.

**Name**

a3 is a contraction of "aaa", which is a recursive acronym for "a3 ain't an assembler".

**Intended use**

a3 is intended to be an assembler which will run on [sOSage](https://gitlab.com/penguin-coders/sosage), although currently the source code has only been compiled with gcc on Linux. To compile a3 to run on [sOSage](https://gitlab.com/penguin-coders/sosage), it will be necessary to extend the C library functions on [sOSage](https://gitlab.com/penguin-coders/sosage), but this is still on the to do list at this stage of development. 

[sOSage](https://gitlab.com/penguin-coders/sosage) is an example OS for educational / hobbyist purposes.

## Instructions

So far, the following instructions have been implemented:
- mov
- add
- adc
- sub
- sbb
- or
- and
- xor
- cmp
- inc
- dec
- push (for general purpose registers)
- pop (for general purpose registers)
- jo
- jno
- jb   
- jnb  
- jz 
- jnz
- jbe
- ja
- js
- jns
- jp
- jnp
- jl
- jnl
- jle
- jnle

## Testing

There is test target in Makefile which assembles test code using both nasm and a3 and compares the resulting binaries by hexdumping them and piping into diff. diff will output a patch for each comparison in testcode/diffs.

## Licence and copyright

All a3 code is released under the GNU General Public Licence v3.0. See [LICENCE](LICENSE) for further information.

&copy; 2022 Mark Williams 
